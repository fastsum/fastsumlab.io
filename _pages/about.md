---
layout: single
permalink: /about/
---

# About the code

{{site.data.names.code}} is a lattice code which implements anisotropic actions
and stout smearing. The code is based off of openQCD 1.6 from which it acquires
openQCD's efficiency, state of the art algorithms, and reliability.

On top of implementing these two additional features, {{site.data.names.code}}
also attempts to improve upon aspects of the code base to make it easier to
develop on in the future. These include issues such as const correctness, a
better build system, and will in the future also aim at implementing CI as well
as turning openQCD into a library so that one can easily take advantage of
openQCD's advanced algorithms when implementing new features or small standalone
programs.

To see code development you can browser the code [changelogs](/changelogs).

# About FASTSUM

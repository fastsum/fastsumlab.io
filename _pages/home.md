---
layout: splash
permalink: /
header:
  overlay_color: "#333"
  cta_label: "<i class='fas fa-download'></i> Download now"
  cta_url: "https://gitlab.com/fastsum/openqcd-fastsum-doc/repository/master/archive.tar.gz"
excerpt: 'Anisotropy and stout smearing for openQCD 1.6<br />
<small><a href="https://gitlab.com/fastsum/openqcd-fastsum/tree/v1.0">Latest release v1.0</a></small>'
---

Welcome to {{ site.data.names.code }}, an extension of the renowned
<a href="http://luscher.web.cern.ch/luscher/openQCD/">openQCD 1.6</a> lattice
QCD code developed by members of the FASTSUM lattice collaboration.

{{ site.data.names.code }} has all of the capabilities of openQCD but implements
two new features

 - Anisotropic actions
 - Stout smearing

## Getting started

### Downloading the source

The source code is available on GitLab where one can either clone the repository
or download a pre made zip.

<a
  href="{{ site.data.urls.download }}"
  class="btn btn--primary btn--large">
  <i class='fas fa-download'></i> Download v1.0
</a>

To clone the <a href="{{ site.data.urls.repo }}">GitLab repository</a>

```bash
git clone {{ site.data.urls.clone }}
```

This has the added benefit that the program logfile will contain a git SHA which
can later be used to reference which version of the code the results were
generated with.

In this repository the `master` branch points to the latest release while the
`devel` branch contains the most up to date (possibly broken) version. The
individual releases are also tagged e.g. `v1.0`.


### Building

To build the code one can simply run `make` in the build directory:

```
cd build
make
```

However, one probably wants to
configure the build prior to this by editing the
[compile_settings.txt]({{site.data.urls.tree}}/build/compile_settings.txt)
file. It is also possible to copy the build directory elsewhere if one wishes to
keep multiple builds. For information on how the build system works, which
targets are available, and which flags are supported, please see the
[build documentation](/documentation/build-system/).


### Configuring the action

The {{ site.data.names.code }} code is compatible with all openQCD 1.4 and 1.6
configurations and accept the following additional (_optional_) blocks

#### Anisotropy

```
[Anisotropy parameters]
use_tts       0
nu            1.5
xi            4.3
cR            1.5
cT            0.9
us_gauge      0.7
ut_gauge      1.0
us_fermion    1.0
ut_fermion    1.0
```

#### Stout smearing

```
[Smearing parameters]
n_smear       2
rho_s         0.14
rho_t         0.00
gauge         0
fermion       1
```

For more information on how the parameters are defined, please see the
[documentation](/documentation/).

## Bugs and issues

If you find any bugs please report them to the repository issue tracker

<p>
  <a
    href="{{ site.data.urls.issue }}"
    class="btn btn--primary btn--large">
    <i class='fab fa-gitlab'></i> Register new issue
  </a>
</p>

Here you can also see known bugs and hopefully get a sense of its status. You
can also send us an email with the issue you are having.

## Authors and License

{{ site.data.names.code }} is developed by {{ site.data.names.jonas }} and
{{ site.data.names.benjamin }}, and licensed under the GNU Public License (GPL).

openQCD 1.6 was developed by Martin Lüscher and Stefan Schaefer with
contributions from others (see homepage, also GPL license).

{{ site.data.names.code }} also include
<a href="http://hpc.desy.de/simlab/codes/openqcd_bgopt/">optimisations for BlueGeneQ</a>
developed by Dalibor Djukanovic, Mauro Papinutto, and Hubert Simma, as well as
additional
<a href="https://sa2c-gitlab.swansea.ac.uk/j.m.o.rantaharju/openqcd_vector512">AVX512 vectorisation</a>
instructions developed by Michele Messiti and Jarno Rantaharju of the
<a href="https://www.swansea.ac.uk/iss/sa2c/">SA2C</a>.

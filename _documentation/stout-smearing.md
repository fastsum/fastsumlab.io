---
title: "Stout smearing for openQCD"
excerpt: "Stout smearing method definition, parameter specification and
programmatic implementation details."
tags:
 - v1.0
---

<a
  href="{{site.data.urls.tree}}/doc/stout_smearing.pdf"
  class="btn btn--primary btn--large">
  <i class="fas fa-book"></i> Download pdf
</a>

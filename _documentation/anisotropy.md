---
title: "Anisotropic actions for openQCD"
excerpt: "Anisotropic action definition, parameter specification and
implementation details."
tags:
 - v1.0
---

<a
  href="{{site.data.urls.raw}}/doc/anisotropy.pdf"
  class="btn btn--primary btn--large">
  <i class="fas fa-book"></i> Download pdf
</a>
